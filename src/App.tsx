import { useReducer } from "react";

interface State {
  count: number;
  message: string;
}

type Action =
  | { type: "count"; add: number }
  | { type: "message"; input: string };

const reducer = (state: State, action: Action) => {
  switch (action.type) {
    case "count":
      return { count: state.count + action.add, message: state.message };

    case "message":
      return { count: state.count, message: action.input };
  }
};

function App(): JSX.Element {
  const [state, dispatch] = useReducer(reducer, { count: 0, message: "" });

  const increment = () => {
    dispatch({ type: "count", add: 1 });
  };
  const decrement = () => {
    dispatch({ type: "count", add: -1 });
  };

  return (
    <>
      <button onClick={decrement}>-</button>
      {state.count}
      <button onClick={increment}>+</button>
      <input
        value={state.message}
        onChange={(event) =>
          dispatch({ type: "message", input: event.target.value })
        }
      />
      {state.message}
    </>
  );
}

export default App;
